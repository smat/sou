var lastkeyword = "";
$(function(){
	$("#keyword").keyup(function(e){
		var keyCode = e.keyCode || e.which;
		if(keyCode == 38 || keyCode == 40) {
			e.preventDefault();
			return;
		}
		
		//回车键
		if(keyCode == 13) {
			$("#searchBtn").click();
			return;
		}
		
		if(keyCode == 116) {
			return;
		}
		
		var keyword = $(this).val();
		if(!keyword || lastkeyword == keyword) {
			$("#keywordSug").hide();
			return;
		}
		keyword = keyword.replace(/(^\s*)|(\s*$)/g, "");
		var baiduSugUrl = "http:\/\/suggestion.baidu.com/su?wd="+keyword+"&amp;sc=hao123&amp;t=" + new Date().getTime();
		var script = $("<script class='script'><\/script>").attr("src", baiduSugUrl);
		$("#keywordSug").show();
		$("#script").html(script);
	}).keydown(function(e){
		var keyCode = e.keyCode || e.which;
		var selected = $("#keywordSug").find(".selected");
		if($("#keywordSug").is(':hidden')) return;
		if(keyCode == 38) {
			var prev = selected.prev();
			if(selected.length > 0) {
				selected.removeClass("selected");
				if(prev.length > 0) {
					prev.addClass("selected");
				} else {
					prev = $("#keywordSug li").last().addClass("selected");
				}
			} else {
				prev = $("#keywordSug li").last().addClass("selected");
			}
			$("#keyword").val(prev.text());
			e.preventDefault();
		}
		
		if(keyCode == 40) {
			var next = selected.next();
			if(selected.length > 0) {
				selected.removeClass("selected");
				if(next.length > 0) {
					next.addClass("selected");
				} else {
					next = $("#keywordSug li:eq(0)").addClass("selected");
				}
			} else {
				next = $("#keywordSug li:eq(0)").addClass("selected");
			}
			$("#keyword").val(next.text());
			e.preventDefault();
		}
	});
	
	$(window).click(function(){
		$("#keywordSug").hide();
	});
});

window.baidu = {
	sug: function(data) {
		var keySugs = [];
		var sug = data.s;
		for(var i=0; i<sug.length; i++) {
			keySugs.push("<li>" + sug[i] + "</li>");
		}
		$("#keywordSug").html(keySugs.join(""));
		lastkeyword = data.q;
		$("#keywordSug li").click(function(){
			var text = $(this).text();
			$("#keywordSug").hide();
			$("#keyword").val(text);
			$("#searchBtn").click();
		}).hover(function(){
			$("#keywordSug li").removeClass("selected");
			$(this).addClass("selected");
			//$("#keyword").val($(this).text());
		});
	}
};