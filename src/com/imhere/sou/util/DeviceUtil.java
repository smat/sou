package com.imhere.sou.util;

import javax.servlet.http.HttpServletRequest;

public class DeviceUtil {

	private final static String[] USER_AGENT = { "Android", "iPhone", "iPod", "iPad", "Windows Phone", "MQQBrowser" };

	/**
	 * 判断用户是否使用手机访问
	 * 
	 * @param request
	 */
	public static boolean isRequestFromMobile(HttpServletRequest request) {
		String userAgent = request.getHeader("User-Agent");
		if (StringUtil.isEmpty(userAgent)) return true;
		boolean isMobile = false;
		if (!userAgent.contains("Windows NT") || 
				(userAgent.contains("Windows NT") && userAgent.contains("compatible; MSIE 9.0;"))) {
			// 排除 苹果桌面系统
			if (!userAgent.contains("Windows NT") && !userAgent.contains("Macintosh")) {
				for (String item : USER_AGENT) {
					if (userAgent.contains(item)) {
						isMobile = true;
						break;
					}
				}
			}
		}
		return isMobile;
	}

}
