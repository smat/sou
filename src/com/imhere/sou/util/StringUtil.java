package com.imhere.sou.util;

public class StringUtil {
	
	public static boolean isEmpty(String str){
		if(str == null) return true;
		if("".equals(str)) return true;
		if("".equals(str.trim())) return true;
		return false;
	}
	
	public static boolean isNotEmpty(String str){
		return !isEmpty(str);
	}
}
