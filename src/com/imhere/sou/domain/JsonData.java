package com.imhere.sou.domain;

public class JsonData {
	
	private String status = "1";
	
	private String msg;
	
	private Object data;
	
	public JsonData setSuccessMsg(String msg){
		this.status = "1";
		this.msg = msg;
		return this;
	}
	
	public JsonData setErrorMsg(String msg){
		this.status = "0";
		this.msg = msg;
		return this;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
