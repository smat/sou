package com.imhere.sou.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 获取手机端首页新闻
 * @author 大雄博客 <a href="http://blog.here325.com">http://blog.here325.com</a>
 *
 */
public class APIForGetData extends HttpServlet {
	private static final long serialVersionUID = 5978580174701101562L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//apiKey=5c207acb280a18940d707b8320da6559
		//channelId=5572a108b3cdc86cf39001d1
		//channelName=互联网焦点
		
		/**
		 * 说明：这里是移动端首页推荐阅读的新闻来源，此处用到的apikey是我个人申请的，
		 * 如果你也需要，烦请自己去申请，或者自己改造这里的数据来源。
		 */
		String httpUrl = "http://apis.baidu.com/showapi_open_bus/channel_news/search_news";
		
		String page = request.getParameter("page");
		if(page == null) page = "1";
		
		String maxResult = "8";
		String pageSize = request.getParameter("pageSize");
		if(pageSize != null) maxResult = pageSize;
		
		String httpArg = "channelId=5572a109b3cdc86cf39001db&channelName=国内最新&page=" + page + "&needHtml=0&needContent=0&needAllList=0&maxResult=" + maxResult;
		
		String json = request(httpUrl, httpArg);
		response.setContentType("application/json;charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.write(json);
	}
	
	//请求接口
	private static String request(String httpUrl, String httpArg) {
	    BufferedReader reader = null;
	    String result = "{}";
	    StringBuffer sbf = new StringBuffer();
	    httpUrl = httpUrl + "?" + httpArg;

	    try {
	        URL url = new URL(httpUrl);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setRequestMethod("GET");
	        
	        /**
			 * 说明：这里是移动端首页推荐阅读的新闻来源，此处用到的apikey是我个人申请的，
			 * 如果你也需要，烦请自己去申请，或者自己改造这里的数据来源。
			 */
	        
	        // 填入apikey到HTTP header
	        connection.setRequestProperty("apikey",  "xxxxxxxxxxxxxxxxxxxxxxx");
	        connection.connect();
	        InputStream is = connection.getInputStream();
	        reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	        String strRead = null;
	        while ((strRead = reader.readLine()) != null) {
	            sbf.append(strRead);
	            sbf.append("\r\n");
	        }
	        reader.close();
	        result = sbf.toString();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return result;
	}
}
